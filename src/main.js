import Vue from "vue";
import App from "./App.vue";
import VueRouter from "vue-router";

import Home from "./pages/Home.vue";
import Location from "./pages/Location.vue";
import SearchResults from "./pages/SearchResults.vue";

require("./assets/app.css");

Vue.config.productionTip = false;
Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { path: "/", component: Home },
    { path: "/weather/:woeid", component: Location },
    { path: "/search/:keyword", component: SearchResults }
  ]
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
