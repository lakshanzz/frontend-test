import axios from "axios";

const API_BASE = "http://localhost/weather_app/weather.php";

export const WeatherApiService = {
  GetByWoeid(woeid) {
    return axios.get(API_BASE + "?command=location&woeid=" + woeid);
  },

  SearchLocations(keyword) {
    return axios.get(API_BASE + "?command=search&keyword=" + keyword);
  }
};
